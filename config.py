import sqlite3
import time
import math
import re
from flask import Flask, render_template, url_for, flash


class FDataBase:
    def __init__(self, db):
        self.__db = db
        self.__cur = db.cursor()

    def getMenu(self):
        sql = """ SELECT * FROM mainmenu """
        try:
            self.__cur.execute(sql)
            res = self.__cur.fetchall()
            if res: return res
        except:
            print('Ошибка чтения из Базы Данных')
        return []

    def addUser(self, username, email, hpsw):
        try:
            self.__cur.execute(f'SELECT COUNT() as `count` FROM users WHERE email LIKE "{email}"')
            res = self.__cur.fetchone()
            self.__cur.execute(f'SELECT COUNT() as `count1` FROM users WHERE username LIKE "{username}"')
            res1 = self.__cur.fetchone()
            if res1['count1'] > 0:
                flash('Пользователь с таким Логин уже существует', 'error')
                return False
            elif res['count'] > 0:
                flash('Пользователь с таким Email уже существует', 'error')
                return False

            tm = math.floor(time.time())
            self.__cur.execute('INSERT INTO users VALUES(NULL, ?, ?, ?, NULL, ?)', (username, email, hpsw, tm))
            self.__db.commit()
        except sqlite3.Error as e:
            print('Ошибка добавления пользователя в БД '+str(e))
            return False

        return True

    def getUser(self, user_id):
        try:
            self.__cur.execute(f'SELECT * FROM users WHERE id = {user_id} LIMIT 1')
            res = self.__cur.fetchone()
            if not res:
                print('Пользователь не найден в БД')
                return False

            return res
        except sqlite3.Error as e:
            print('Ошибка получения данных из БД' + str(e))

        return False

    def getUserByLogin(self, username):
        try:
            self.__cur.execute(f"SELECT * FROM users WHERE username = '{username}' LIMIT 1")
            res = self.__cur.fetchone()
            if not res:
                print('Пользователь не найден в БД')
                return False

            return res
        except sqlite3.Error as e:
            print('Ошибка получения данных из БД' + str(e))
        return False

    def updateUserAvatar(self, avatar, user_id):
        if not avatar:
            return False

        try:
            binary = sqlite3.Binary(avatar)
            self.__cur.execute(f'UPDATE users SET avatar = ? WHERE id = ?', (binary, user_id))
            self.__db.commit()
        except sqlite3.Error as e:
            print('Ошибка обновления аватара в БД: '+str(e))
            return False
        return True
