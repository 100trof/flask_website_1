import sqlite3
import os
import time
import re
from flask import Flask, render_template, url_for, flash, session, redirect, request, abort, g, make_response
from config import FDataBase
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, login_user, login_required, logout_user, current_user
from UserLogin import UserLogin


DATABASE = 'all_data.db'
DEBUG = True
SECRET_KEY = 's1o3b5a7k9a11k2u4s6a8k10a12'
MAX_CONTENT_LENGTH = 1024 * 1024

app = Flask(__name__)
app.config.from_object(__name__)
app.config.update(dict(DATABASE=os.path.join(app.root_path, 'all_data.db')))

login_manager = LoginManager(app)
login_manager.login_view = 'login_page'
login_manager.login_message = 'Пройдите, пожалуйста, авторизацию для доступа к шикарному контенту'
login_manager.login_message_category = 'error'


@login_manager.user_loader
def load_user(user_id):
    print('load_user - авторизация')
    return UserLogin().fromDB(user_id, dbase)

def connect_db():
    conn = sqlite3.connect(app.config['DATABASE'])
    conn.row_factory = sqlite3.Row
    return conn


def create_db():
    db = connect_db()
    with app.open_resource('sq_db.sql', mode='r') as file_handler:
        db.cursor().executescript(file_handler.read())
    db.commit()
    db.close()


create_db()


def get_db():
    if not hasattr(g, 'link_db'):
        g.link_db = connect_db()
    return g.link_db


dbase = None
@app.before_request
def before_request():
    global dbase
    db = get_db()
    dbase = FDataBase(db)


@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'link_db'):
        g.link_db.close()


@app.route('/')
def general_page():
    return render_template('general_page.html', menu=dbase.getMenu())


@app.route('/profile')
@login_required
def profile():
    return render_template('profile.html', menu=dbase.getMenu(), title='Профиль')


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page404.html', title='Страница не найдена', menu=dbase.getMenu()), 404


@app.route('/login', methods=['POST', 'GET'])
def login_page():
    if current_user.is_authenticated:
        return redirect(url_for('profile'))

    if request.method == 'POST':
        user = dbase.getUserByLogin(request.form['username'])
        if user and check_password_hash(user['psw'], request.form['psw']):
            userlogin = UserLogin().create(user)
            remember_me = True if request.form.get('remainme') else False
            login_user(userlogin, remember=remember_me)
            return redirect(url_for('profile'))

        flash('Неверная пара логин/пароль', 'error')

    return render_template('login.html', menu=dbase.getMenu(), title='Авторизация')


@app.route('/signup', methods=['POST', 'GET'])
def signup_page():
    if request.method == 'POST':
        if request.form['psw'] != request.form['psw2']:
            flash('Введенные пароли не совпадают', 'error')
        elif len(request.form['username']) > 16:
            flash('Слишком длинный Логин (max 15 символов)', 'error')
        elif len(request.form['email']) > 16:
            flash('Слишком длинный Email (max 15 символов)', 'error')
        elif (len(request.form['username']) > 3 and len(request.form['email']) > 3
                and len(request.form['psw']) > 3 and request.form['psw'] == request.form['psw2']):
            hash = generate_password_hash(request.form['psw'])
            res = dbase.addUser(request.form['username'], request.form['email'], hash)
            if res:
                flash('Регистрация прошла успешно', 'success')
                return redirect(url_for('login_page'))
            else:
                flash('Ошибка при добавлении в БД', 'error')

        else:
            flash('Неверно заполнены поля (min 4 символа)', 'error')

    return render_template('signup.html', menu=dbase.getMenu(), title='Регистрация')


@app.route('/logout')
@login_required
def logout_page():
    logout_user()
    flash('Вы успешно вышли из аккаунта', 'success')
    return redirect(url_for('login_page'))


@app.route('/userava')
@login_required
def userava():
    img = current_user.getAvatar(app)
    if not img:
        return ''

    h = make_response(img)
    h.headers['Content-Type'] = 'image/png'
    return h


@app.route('/upload', methods=['POST', 'GET'])
@login_required
def upload():
    if request.method == 'POST':
        file = request.files['file']
        if file and current_user.verifyExt(file.filename):
            try:
                img = file.read()
                res = dbase.updateUserAvatar(img, current_user.get_id())
                if not res:
                    flash('Ошибка обновления аватара', 'error')
                flash('Аватар обновлён', 'success')
            except FileNotFoundError as e:
                flash('Ошибка чтения файла', 'error')
        else:
            flash('Ошибка обновления аватара', 'error')

    return redirect(url_for('profile'))


if __name__ == '__main__':
    app.run(debug=True)
